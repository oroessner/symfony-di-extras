<?php
declare(strict_types=1);

namespace Tests\Basster\SymfonyDiExtras\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Class TestEvent
 *
 * @package Tests\Basster\SymfonyDiExtras\Event
 */
final class TestEvent extends Event
{
}

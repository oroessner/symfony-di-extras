<?php

declare(strict_types=1);

namespace Tests\Basster\SymfonyDiExtras\Event;

use Basster\SymfonyDiExtras\Event\NullDispatcher;
use PHPUnit\Framework\TestCase;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\GenericEvent;

/**
 * Class NullDispatcherTest.
 *
 * @internal
 * @coversNothing
 */
final class NullDispatcherTest extends TestCase
{
    private const SOME_EVENT = 'some-event';

    /** @var NullDispatcher */
    private $dispatcher;

    protected function setUp(): void
    {
        $this->dispatcher = new NullDispatcher();
    }

    public function testDispatchReturnsGivenEvent(): void
    {
        $event = new GenericEvent();
        $this->assertSame($event, $this->dispatcher->dispatch($event));
    }

    public function testGetListenersAlwaysReturnsAnEmptyArray(): void
    {
        $this->addSomeListener();
        $this->assertSame([], $this->dispatcher->getListeners());
    }

    public function testHasListenersAlwaysReturnsFalse(): void
    {
        $this->addSomeListener();
        $this->assertFalse($this->dispatcher->hasListeners());
    }

    public function testGetListenerPriorityAlwaysReturnsNull(): void
    {
        $this->assertNull($this->dispatcher->getListenerPriority(self::SOME_EVENT, $this->createDummyFunction()));
    }

    /**
     * adds a dummy function as listener to the NullDispatcher.
     */
    private function addSomeListener(): void
    {
        $this->dispatcher->addListener(
            self::SOME_EVENT,
            $this->createDummyFunction()
        );
    }

    /**
     * @return \Closure
     */
    private function createDummyFunction(): callable
    {
        return static function () {
            return false;
        };
    }
}

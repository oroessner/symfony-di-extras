<?php

declare(strict_types=1);

namespace Tests\Basster\SymfonyDiExtras\Messenger;

use Basster\SymfonyDiExtras\Messenger\MessageBusAwareInterface;
use Basster\SymfonyDiExtras\Messenger\MessageBusAwareTrait;
use Symfony\Component\Messenger\Envelope;

/**
 * Class MessageBusAwareDummy.
 */
class MessageBusAwareDummy implements MessageBusAwareInterface
{
    use MessageBusAwareTrait;

    public function doDispatchMessage($message): Envelope
    {
        return $this->dispatchMessage($message);
    }
}

<?php

declare(strict_types=1);

namespace Tests\Basster\SymfonyDiExtras\DependencyInjection;

use Basster\SymfonyDiExtras\DependencyInjection\IterableServiceLocator;
use PHPUnit\Framework\TestCase;

/**
 * Class IterableServiceLocatorTest.
 *
 * @internal
 * @coversNothing
 */
final class IterableServiceLocatorTest extends TestCase
{
    public function testIterateOverServiceKeys(): void
    {
        $factory = function () {
            $obj = new \stdClass();
            $obj->foo = 'bar';

            return $obj;
        };
        $container = new IterableServiceLocator(['foo' => $factory]);

        foreach ($container as $key) {
            $this->assertSame('foo', $key);
        }
        $this->assertSame('bar', $container->get('foo')->foo);
    }

    public function testTheIteratorCoversTheKeys(): void
    {
        $container = new IterableServiceLocator(['foo' => function (): void {
        }, 'bar' => function (): void {
        }]);

        $this->assertSame(['foo', 'bar'], iterator_to_array($container));
    }
}

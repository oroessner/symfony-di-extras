<?php

declare(strict_types=1);

namespace Basster\SymfonyDiExtras\Messenger;

use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Class NullBus.
 */
final class NullBus implements MessageBusInterface
{
    /**
     * {@inheritdoc}
     *
     * @codeCoverageIgnore
     */
    public function dispatch($message, array $stamps = []): Envelope
    {
        return new Envelope(new \stdClass());
    }
}

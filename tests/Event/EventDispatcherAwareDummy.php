<?php

declare(strict_types=1);

namespace Tests\Basster\SymfonyDiExtras\Event;

use Basster\SymfonyDiExtras\Event\EventDispatcherAwareInterface;
use Basster\SymfonyDiExtras\Event\EventDispatcherAwareTrait;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Class EventDispatcherAwareDummy.
 */
class EventDispatcherAwareDummy implements EventDispatcherAwareInterface
{
    use EventDispatcherAwareTrait;

    public function doDispatchEvent(string $eventName, Event $event = null)
    {
        return $this->dispatchEvent($eventName, $event);
    }

    public function doDispatchEventTheNewWay($event)
    {
        return $this->eventDispatcher->dispatch($event);
    }
}

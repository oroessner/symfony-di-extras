<?php

declare(strict_types=1);

namespace Basster\SymfonyDiExtras\Messenger;

use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\StampInterface;

/**
 * Class MessageBusAwareTrait.
 */
trait MessageBusAwareTrait
{
    /** @var MessageBusInterface */
    protected $messageBus;

    public function setMessageBus(MessageBusInterface $messageBus): void
    {
        $this->messageBus = $messageBus;
    }

    /**
     * Convenience function to dispatch an event.
     *
     * @param object|Envelope  $message The message or the message pre-wrapped in an envelope
     * @param StampInterface[] $stamps
     *
     * @return mixed
     *
     * @see MessageBusInterface::dispatch()
     */
    protected function dispatchMessage($message, array $stamps = []): Envelope
    {
        return $this->messageBus->dispatch($message, $stamps);
    }
}

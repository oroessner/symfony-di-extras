<?php

declare(strict_types=1);

namespace Tests\Basster\SymfonyDiExtras\Messenger;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Class MessageBusAwareTraitTest.
 *
 * @internal
 * @coversNothing
 */
final class MessageBusAwareTraitTest extends TestCase
{
    public function testDispatchMessage(): void
    {
        $message = new \stdClass();
        $stamps = [];
        $messageBus = $this->prophesize(MessageBusInterface::class);

        $dummy = new MessageBusAwareDummy();
        $dummy->setMessageBus($messageBus->reveal());

        $messageBus->dispatch($message, $stamps)->shouldBeCalled()->willReturn(new Envelope($message));

        $dummy->doDispatchMessage($message);
    }
}

<?php

declare(strict_types=1);

namespace Basster\SymfonyDiExtras\DependencyInjection;

use Iterator;
use Symfony\Component\DependencyInjection\ServiceLocator;

/**
 * Class IterableServiceLocator.
 */
class IterableServiceLocator extends ServiceLocator implements Iterator
{
    /** @var array stores the factory keys */
    private $keys;

    private $position = 0;

    public function __construct(array $factories)
    {
        $this->keys = array_keys($factories);
        parent::__construct($factories);
    }

    /** {@inheritdoc} */
    public function current()
    {
        return $this->keys[$this->position];
    }

    /** {@inheritdoc} */
    public function next(): void
    {
        ++$this->position;
    }

    /** {@inheritdoc} */
    public function key()
    {
        return $this->position;
    }

    /** {@inheritdoc} */
    public function valid()
    {
        return isset($this->keys[$this->position]);
    }

    /** {@inheritdoc} */
    public function rewind(): void
    {
        $this->position = 0;
    }
}

<?php

declare(strict_types=1);

namespace Basster\SymfonyDiExtras\Messenger;

use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Class MessengerDispatcherAware.
 */
interface MessageBusAwareInterface
{
    /**
     * @param \Symfony\Component\Messenger\MessageBusInterface $messageBus
     */
    public function setMessageBus(MessageBusInterface $messageBus): void;
}

# symfony-di-extras

[![Latest Stable Version](https://poser.pugx.org/basster/symfony-di-extras/v/stable)](https://packagist.org/packages/basster/symfony-di-extras) [![Total Downloads](https://poser.pugx.org/basster/symfony-di-extras/downloads)](https://packagist.org/packages/basster/symfony-di-extras) [![License](https://poser.pugx.org/basster/symfony-di-extras/license)](https://packagist.org/packages/basster/symfony-di-extras) [![pipeline status](https://gitlab.com/oroessner/symfony-di-extras/badges/master/pipeline.svg)](https://gitlab.com/oroessner/symfony-di-extras/commits/master) [![coverage report](https://gitlab.com/oroessner/symfony-di-extras/badges/master/coverage.svg)](https://gitlab.com/oroessner/symfony-di-extras/commits/master)
 [![SensioLabsInsight](https://insight.sensiolabs.com/projects/0222083a-1450-4e8f-837a-0e0135ff4676/mini.png)](https://insight.sensiolabs.com/projects/0222083a-1450-4e8f-837a-0e0135ff4676) 
 
This is a collection of classes and interfaces which I reuse in many projects to help me injecting frequently used services in my own services.
 
## Installation

Version 2 follows the [simpler event dispatching approach of Symfony 4.3](https://symfony.com/blog/new-in-symfony-4-3-simpler-event-dispatching).

```bash
composer req basster/symfony-di-extras
```

Prior to Symfony 4.3, please use:
```bash
composer req basster/symfony-di-extras:^1.6
```
 
## EventDispatcherAwareInterface
 
To inject the event-dispatcher into a service, let it implement the [EventDispatcherAwareInterface](src/Event/EventDispatcherAwareInterface.php). With version 3.3 Symfony introduced [Interface-based service configuration](https://symfony.com/blog/new-in-symfony-3-3-simpler-service-configuration#interface-based-service-configuration), so you can utilize it, to automatically perform di-actions based in the type of class/implemented interface. Here's an example:
 
 ```yaml
# app/config/services.yml or config/services.yaml (Symfony Flex)
services:
  _instanceof:
    Basster\SymfonyDiExtras\Event\EventDispatcherAwareInterface:
      calls:
      - [setEventDispatcher, ['@event_dispatcher']]
 ```
 
For not implementing the `setEventDispatcher` method by yourself, you can `use` the [EventDispatcherAwareTrait](src/Event/EventDispatcherAwareTrait.php) which also holds a small convenience method `dispatchEvent(string, Symfony\Component\EventDispatcher\Event)`

Since this kind of (setter) injection makes the EventDispatcher optional on the implementing service, you make yourself dependent of the used di-container, whether the service will be injected or not. To avoid cluttering your own code with `if ($this->eventDispatcher) { $this->dispatchEvent(...); }` calls, I included the [NullDispatcher](src/Event/NullDispatcher.php) which follows the [Null Object Pattern](https://en.wikipedia.org/wiki/Null_object_pattern) implementing the `Symfony\Component\EventDispatcher\EventDispatcherInterface`.

I normally assign it on the constructor of the class implementing the `EventDispatcherAwareInterface`.

Example:

```php
<?php

use Basster\SymfonyDiExtras\Event\EventDispatcherAwareInterface;
use Basster\SymfonyDiExtras\Event\EventDispatcherAwareTrait;
use Basster\SymfonyDiExtras\Event\NullDispatcher;

class MyService implements EventDispatcherAwareInterface
{
    use EventDispatcherAwareTrait;
    
    public function __construct() {
        $this->setEventDispatcher(new NullDispatcher());   
    }
}
```

## MessageBusAware
 
To inject the message bus into a service, let it implement the [MessageBusAwareInterface](src/Messenger/MessageBusAwareInterface.php).
 
 ```yaml
# app/config/services.yml or config/services.yaml (Symfony Flex)
services:
  _instanceof:
    Basster\SymfonyDiExtras\Event\MessageBusAware:
      calls:
      - [setMessageBus, ['@message_bus']]
 ```
 
For not implementing the `setMessageBus` method by yourself, you can `use` the [MessageBusAwareTrait](src/Messenger/MessageBusAwareTrait.php) which also holds a small convenience method `dispatchMessage(mixed)`

Since this kind of (setter) injection makes the EventBus optional on the implementing service, you make yourself dependent of the used di-container, whether the service will be injected or not. To avoid cluttering your own code with `if ($this->messageBus) { $this->messageBus->dispatch(...); }` calls, I included the [NullBus](src/Messenger/NullBus.php) which follows the [Null Object Pattern](https://en.wikipedia.org/wiki/Null_object_pattern) implementing the `Symfony\Component\Messenger\MessageBusInterface`.

I normally assign it on the constructor of the class implementing the `MessageBusAware`.

Example:

```php
<?php

use Basster\SymfonyDiExtras\Messenger\MessageBusAwareInterface;
use Basster\SymfonyDiExtras\Messenger\MessageBusAwareTrait;
use Basster\SymfonyDiExtras\Messenger\NullBus;

class MyService implements MessageBusAware
{
    use EventDispatcherAwareTrait;
    
    public function __construct() {
        $this->setMessageBus(new NullBus());   
    }
}
```

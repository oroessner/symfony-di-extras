<?php

declare(strict_types=1);

namespace Basster\SymfonyDiExtras\Event;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class NullDispatcher.
 */
final class NullDispatcher implements EventDispatcherInterface
{
    /** {@inheritdoc} */
    public function dispatch($event)
    {
        return $event;
    }

    /**
     * {@inheritdoc}
     *
     * @codeCoverageIgnore
     */
    public function addListener($eventName, $listener, $priority = 0): void
    {
    }

    /**
     * {@inheritdoc}
     *
     * @codeCoverageIgnore
     */
    public function addSubscriber(EventSubscriberInterface $subscriber): void
    {
    }

    /**
     * {@inheritdoc}
     *
     * @codeCoverageIgnore
     */
    public function removeListener($eventName, $listener): void
    {
    }

    /**
     * {@inheritdoc}
     *
     * @codeCoverageIgnore
     */
    public function removeSubscriber(EventSubscriberInterface $subscriber): void
    {
    }

    /** {@inheritdoc} */
    public function getListeners($eventName = null): array
    {
        return [];
    }

    /** {@inheritdoc} */
    public function getListenerPriority($eventName, $listener): ?int
    {
        return null;
    }

    /** {@inheritdoc} */
    public function hasListeners($eventName = null): bool
    {
        return false;
    }
}

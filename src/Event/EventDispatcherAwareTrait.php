<?php

declare(strict_types=1);

namespace Basster\SymfonyDiExtras\Event;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\LegacyEventDispatcherProxy;

/**
 * Class EventDispatcherAwareTrait.
 */
trait EventDispatcherAwareTrait
{
    /** @var \Symfony\Component\EventDispatcher\EventDispatcherInterface */
    protected $eventDispatcher;

    public function setEventDispatcher(EventDispatcherInterface $eventDispatcher): void
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * Convenience function to dispatch an event.
     * Utilizes the LegacyEventDispatcherProxy.
     *
     * @param object $event          The event to pass to the event handlers/listeners
     * @param string $eventName      The name of the event to dispatch. If not supplied,
     *                               the class of $event should be used instead.
     *
     * @return object The passed $event MUST be returned
     */
    protected function dispatchEvent(string $eventName, $event)
    {
        $eventDispatcher = LegacyEventDispatcherProxy::decorate($this->eventDispatcher);

        return $eventDispatcher->dispatch($event, $eventName);
    }
}

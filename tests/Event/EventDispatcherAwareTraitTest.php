<?php

declare(strict_types=1);

namespace Tests\Basster\SymfonyDiExtras\Event;

use PHPUnit\Framework\TestCase;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;

/**
 * Class EventDispatcherAwareTraitTest.
 *
 * @internal
 * @coversNothing
 */
final class EventDispatcherAwareTraitTest extends TestCase
{
    private const EVENT_NAME = 'some.event';

    private $eventDispatcher;

    private $dummy;

    private $event;

    protected function setUp(): void
    {
        $this->eventDispatcher = $this->prophesize(EventDispatcherInterface::class);

        $this->dummy = new EventDispatcherAwareDummy();
        $this->dummy->setEventDispatcher($this->eventDispatcher->reveal());

        $this->event = new TestEvent();
    }

    /**
     * @covers \Basster\SymfonyDiExtras\Event\EventDispatcherAwareTrait::dispatchEvent()
     * @covers \Basster\SymfonyDiExtras\Event\EventDispatcherAwareTrait::setEventDispatcher()
     */
    public function testDispatchEvent(): void
    {
        $this->eventDispatcher->dispatch($this->event, self::EVENT_NAME)->shouldBeCalled()->willReturn($this->event);

        $this->dummy->doDispatchEvent(self::EVENT_NAME, $this->event);
    }

    public function testNewWayOfDispatchingEvents(): void
    {
        $this->dummy->doDispatchEventTheNewWay($this->event);

        $this->eventDispatcher->dispatch($this->event)->shouldHaveBeenCalled();
    }
}

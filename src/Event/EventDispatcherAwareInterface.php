<?php

declare(strict_types=1);

namespace Basster\SymfonyDiExtras\Event;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class EventDispatcherAwareInterface.
 */
interface EventDispatcherAwareInterface
{
    /**
     * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
     */
    public function setEventDispatcher(EventDispatcherInterface $eventDispatcher): void;
}
